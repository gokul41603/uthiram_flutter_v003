import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class RegisterPage extends StatefulWidget {
  const RegisterPage({Key? key}) : super(key: key);

  @override
  _RegisterPageState createState() => _RegisterPageState();
}

class _RegisterPageState extends State<RegisterPage> {
  final _form = GlobalKey<FormState>();
  CollectionReference reference =
      FirebaseFirestore.instance.collection("userDto");
  Map<String, dynamic> userDto = {};

  void _validateForm() {
    final _isValid = _form.currentState!.validate();

    if (_isValid) {
      if (userDto["departmentName"] != "Department Name") {
        if (userDto["bloodGroup"] != "Blood Group") {
          if (userDto["districtName"] != "District Name") {
            userDto['status'] = "Active";
            userDto['lastDonatedDate'] = "";
            reference.doc(userDto["rollNo"]).set(userDto);
            Navigator.pushNamed(context, "login");
          }
        }
      }
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.pink.shade50,
      appBar: AppBar(
        title: Text("Register as Donor"),
        backgroundColor: Colors.blue.shade900,
      ),
      body: Center(
        child: SingleChildScrollView(
          padding: EdgeInsets.all(10),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.spaceAround,
            children: [
              SizedBox(
                height: 10,
              ),
              Form(
                  key: _form,
                  child: Column(
                    children: [
                      TextFormField(
                          validator: (donorName) {
                            if (donorName!.isEmpty) {
                              return "DonorName can't be Empty";
                            }
                          },
                          onChanged: (donorName) {
                            userDto["donorName"] = donorName;
                          },
                          decoration: InputDecoration(
                              hintText: "Donor Name",
                              filled: true,
                              fillColor: Colors.white,
                              contentPadding: EdgeInsets.symmetric(
                                  vertical: 5.0, horizontal: 32.0),
                              enabledBorder: OutlineInputBorder(
                                borderSide: BorderSide(width: 1),
                                borderRadius: BorderRadius.circular(32.0),
                              ))),
                      SizedBox(
                        height: 20,
                      ),
                      TextFormField(
                          validator: (rollNo) {
                            if (rollNo!.isEmpty) {
                              return "Rollno can't be Empty";
                            }
                          },
                          onChanged: (rollNo) {
                            userDto["rollNo"] = rollNo;
                          },
                          decoration: InputDecoration(
                              hintText: "Donor RollNo",
                              filled: true,
                              fillColor: Colors.white,
                              contentPadding: EdgeInsets.symmetric(
                                  vertical: 5.0, horizontal: 32.0),
                              enabledBorder: OutlineInputBorder(
                                borderSide: BorderSide(width: 1),
                                borderRadius: BorderRadius.circular(32.0),
                              ))),
                      SizedBox(
                        height: 20,
                      ),
                      TextFormField(
                          validator: (phoneNo) {
                            if (phoneNo!.isEmpty) {
                              return "PhoneNo can't be Empty";
                            }
                          },
                          onChanged: (phoneNo) {
                            userDto["phoneNo"] = phoneNo;
                          },
                          decoration: InputDecoration(
                              hintText: "Donor Phone No",
                              filled: true,
                              fillColor: Colors.white,
                              contentPadding: EdgeInsets.symmetric(
                                  vertical: 5.0, horizontal: 32.0),
                              enabledBorder: OutlineInputBorder(
                                borderSide: BorderSide(width: 1),
                                borderRadius: BorderRadius.circular(32.0),
                              ))),
                      SizedBox(
                        height: 20,
                      ),
                      TextFormField(
                          validator: (age) {
                            if (age!.isEmpty) {
                              return "Age can't be Empty";
                            }
                          },
                          onChanged: (age) {
                            userDto["age"] = age;
                          },
                          decoration: InputDecoration(
                              hintText: "Donor Age",
                              filled: true,
                              fillColor: Colors.white,
                              contentPadding: EdgeInsets.symmetric(
                                  vertical: 5.0, horizontal: 32.0),
                              enabledBorder: OutlineInputBorder(
                                borderSide: BorderSide(width: 1),
                                borderRadius: BorderRadius.circular(32.0),
                              ))),
                      SizedBox(
                        height: 20,
                      ),
                      TextFormField(
                          validator: (weight) {
                            if (weight!.isEmpty) {
                              return "Weight can't be Empty";
                            }
                          },
                          onChanged: (weight) {
                            userDto["weight"] = weight;
                          },
                          decoration: InputDecoration(
                              hintText: "Donor Weight",
                              filled: true,
                              fillColor: Colors.white,
                              contentPadding: EdgeInsets.symmetric(
                                  vertical: 5.0, horizontal: 32.0),
                              enabledBorder: OutlineInputBorder(
                                borderSide: BorderSide(width: 1),
                                borderRadius: BorderRadius.circular(32.0),
                              ))),
                      SizedBox(
                        height: 20,
                      ),
                      TextFormField(
                          validator: (address) {
                            if (address!.isEmpty) {
                              return "Address can't be Empty";
                            }
                          },
                          onChanged: (address) {
                            userDto["address"] = address;
                          },
                          decoration: InputDecoration(
                              hintText: "Donor Address",
                              filled: true,
                              fillColor: Colors.white,
                              contentPadding: EdgeInsets.symmetric(
                                  vertical: 5.0, horizontal: 32.0),
                              enabledBorder: OutlineInputBorder(
                                borderSide: BorderSide(width: 1),
                                borderRadius: BorderRadius.circular(32.0),
                              ))),
                      SizedBox(
                        height: 20,
                      ),
                      TextFormField(
                          validator: (pincode) {
                            if (pincode!.isEmpty) {
                              return "Pincode can't be Empty";
                            }
                          },
                          onChanged: (pinCode) {
                            userDto["pinCode"] = pinCode;
                          },
                          decoration: InputDecoration(
                              hintText: "Donor Pincode",
                              filled: true,
                              fillColor: Colors.white,
                              contentPadding: EdgeInsets.symmetric(
                                  vertical: 5.0, horizontal: 32.0),
                              enabledBorder: OutlineInputBorder(
                                borderSide: BorderSide(width: 1),
                                borderRadius: BorderRadius.circular(32.0),
                              ))),
                      SizedBox(
                        height: 20,
                      ),
                      TextFormField(
                          validator: (password) {
                            if (password!.isEmpty) {
                              return "Password can't be Empty";
                            }
                          },
                          onChanged: (password) {
                            userDto["password"] = password;
                          },
                          decoration: InputDecoration(
                              hintText: "Password",
                              filled: true,
                              fillColor: Colors.white,
                              contentPadding: EdgeInsets.symmetric(
                                  vertical: 5.0, horizontal: 32.0),
                              enabledBorder: OutlineInputBorder(
                                borderSide: BorderSide(width: 1),
                                borderRadius: BorderRadius.circular(32.0),
                              ))),
                      SizedBox(
                        height: 20,
                      ),
                      TextFormField(
                          validator: (confirmPassword) {
                            if (confirmPassword!.isEmpty) {
                              return "Password can't be Empty";
                            } else if (userDto['password'] != confirmPassword) {
                              return "Password doesn't match";
                            }
                          },
                          onChanged: (confirmPassword) {
                            userDto["confirmPassword"] = confirmPassword;
                          },
                          decoration: InputDecoration(
                              hintText: "Confirm Password",
                              filled: true,
                              fillColor: Colors.white,
                              contentPadding: EdgeInsets.symmetric(
                                  vertical: 5.0, horizontal: 32.0),
                              enabledBorder: OutlineInputBorder(
                                borderSide: BorderSide(width: 1),
                                borderRadius: BorderRadius.circular(32.0),
                              )))
                    ],
                  )),
              SizedBox(
                height: 20,
              ),
              CupertinoPicker(
                itemExtent: 30,
                backgroundColor: Colors.lightBlueAccent.shade100,
                onSelectedItemChanged: (value) {
                  userDto["departmentName"] = departmentName[value];
                },
                children: departmentList,
              ),
              SizedBox(
                height: 20,
              ),
              CupertinoPicker(
                itemExtent: 30,
                backgroundColor: Colors.lightBlueAccent.shade100,
                onSelectedItemChanged: (bloodGroup) {
                  userDto["bloodGroup"] = bloodGroupName[bloodGroup];
                },
                children: bloodGroup,
              ),
              SizedBox(
                height: 20,
              ),
              CupertinoPicker(
                itemExtent: 30,
                backgroundColor: Colors.lightBlueAccent.shade100,
                onSelectedItemChanged: (districtName) {
                  userDto["districtName"] = district[districtName];
                },
                children: districtName,
              ),
              SizedBox(
                height: 30,
              ),
              MaterialButton(
                elevation: 10,
                minWidth: MediaQuery.of(context).size.width - 40,
                shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(32.0)),
                onPressed: _validateForm,
                color: Colors.red,
                child: Text(
                  "Register",
                  style: TextStyle(color: Colors.white),
                ),
              )
            ],
          ),
        ),
      ),
    );
  }
}

List<Text> departmentList = [
  Text("Department Name"),
  Text("Information Technology"),
  Text("Computer Science"),
  Text("Automobile"),
  Text("Civil"),
  Text("EEE"),
  Text("ECE"),
  Text("Mech"),
  Text("Bio-Tech"),
  Text("FT"),
  Text("TT")
];

List<Text> bloodGroup = [
  Text("Blood Group"),
  Text("B+"),
  Text("B-"),
  Text("O+"),
  Text("O-"),
  Text("A+"),
  Text("A-"),
  Text("AB+"),
  Text("AB-"),
  Text("HH"),
];

List<Text> districtName = [
  Text("District Name"),
  Text("Coimbatore"),
  Text("Tirupur"),
  Text("Erode"),
  Text("Salem"),
  Text("Dharmapuri"),
  Text("Chennai"),
  Text("Ooty"),
  Text("Thiruvalluvar"),
  Text("Tirchy"),
];

List departmentName = [
  "Department Name",
  "Information Technology",
  "Computer Science",
  "Automobile",
  "Civil",
  "EEE",
  "ECE",
  "Mech",
  "Bio-Tech",
  "FT",
  "TT"
];

List district = [
  "District Name",
  "Coimbatore",
  "Tirupur",
  "Erode",
  "Salem",
  "Dharmapuri",
  "Chennai",
  "Ooty",
  "Thiruvalluvar",
  "Tirchy"
];

List bloodGroupName = [
  "Blood Group",
  "B+",
  "B-",
  "O+",
  "O-",
  "A+",
  "A-",
  "AB+",
  "AB-",
  "HH "
];
