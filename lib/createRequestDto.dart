class CreateRequestDto {
  late String patientName;
  late String bloodGroup;
  late String unitsNeeded;
  late String neededWithIn;
  late String hospitalName;
  late String hospitalLocation;
  late String contactNo;

  void setPatientName(String name) {
    patientName = name;
  }

  String getPatientName() {
    return patientName;
  }

  void setBloodGroup(String bloodgroup) {
    this.bloodGroup = bloodgroup;
  }

  String getBloodGroup() {
    return bloodGroup;
  }

  void setUnitsNeeded(String unitsNeeded) {
    this.unitsNeeded = unitsNeeded;
  }

  String getUnitsNeeded() {
    return unitsNeeded;
  }

  void setNeededWith(String neededWithIn) {
    this.neededWithIn = neededWithIn;
  }

  String getNeededWithIn() {
    return neededWithIn;
  }

  void setHospitalName(String hospitalName) {
    this.hospitalName = hospitalName;
  }

  String getHospitalName() {
    return hospitalName;
  }

  void setHospitalLocation(String hospitalLocation) {
    this.hospitalLocation = hospitalLocation;
  }

  String getHospitalLocation() {
    return hospitalLocation;
  }

  void setContactNo(String contactNo) {
    this.contactNo = contactNo;
  }

  String getContactNo() {
    return contactNo;
  }
}
