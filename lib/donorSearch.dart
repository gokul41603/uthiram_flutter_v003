import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_core/firebase_core.dart';
import 'package:flutter/material.dart';
import 'package:flutteruthiramv003/services.dart';
import 'package:flutteruthiramv003/userDto.dart';

class DonorSearch extends StatefulWidget {
  const DonorSearch({Key? key}) : super(key: key);

  @override
  _DonorSearchState createState() => _DonorSearchState();
}

class _DonorSearchState extends State<DonorSearch> {
  TextEditingController _searchController = TextEditingController();
  late Future resultsLoaded;
  List _allresults = [];
  List _resultsList = [];

  @override
  void initState() {
    _searchController.addListener(() {
      _searchTextChanged();
    });
    super.initState();
  }

  @override
  void didChangeDependencies() {
    super.didChangeDependencies();
    resultsLoaded = getUserSearchDetails();
  }

  @override
  void dispose() {
    _searchController.removeListener(() {
      _searchTextChanged();
    });
    _searchController.dispose();
    super.dispose();
  }

  _searchTextChanged() {
    searchResulstsList();
    print(_searchController.text);
  }

  searchResulstsList() {
    var showResults = [];
    if (_searchController.text != "") {
      for (var donorSnapshot in _allresults) {
        var donorName = donorSnapshot['donorName'].toString().toLowerCase();
        print("DonorName : " + donorName);
        if (donorName.contains(_searchController.text.toLowerCase())) {
          showResults.add(donorSnapshot);
          print("donorSnapshot: " + donorSnapshot['donorName']);
        }
      }
    } else {
      showResults = List.from(_allresults);
    }
    setState(() {
      _resultsList = showResults;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: Text("Donor Search"),
        ),
        body: Container(
          padding: EdgeInsets.all(4),
          child: Column(
            children: [
              SizedBox(
                height: 10,
              ),
              TextField(
                controller: _searchController,
                decoration: InputDecoration(
                    prefixIcon: Icon(Icons.search),
                    contentPadding:
                        EdgeInsets.symmetric(vertical: 5.0, horizontal: 32.0),
                    enabledBorder: OutlineInputBorder(
                      borderSide: BorderSide(width: 1),
                      borderRadius: BorderRadius.circular(32.0),
                    )),
              ),
              Expanded(
                child: ListView.builder(
                  itemCount: _resultsList.length,
                  itemBuilder: (context, index) {
                    return Card(
                      elevation: 5,
                      margin: EdgeInsets.all(2),
                      child: Container(
                        height: 120,
                        width: MediaQuery.of(context).size.width - 20,
                        child: Row(
                            mainAxisAlignment: MainAxisAlignment.spaceAround,
                            children: [
                              Column(
                                children: [
                                  SizedBox(
                                    height: 20,
                                  ),
                                  Row(
                                    children: [
                                      // SizedBox(
                                      //   width: 10,
                                      // ),
                                      Text(
                                        "DonorName : " +
                                            _resultsList[index]['donorName'],
                                        style: TextStyle(fontSize: 15),
                                      ),
                                      SizedBox(
                                        width: 30,
                                      ),
                                      Text(
                                        "RollNo : " +
                                            _resultsList[index]['rollNo'],
                                        style: TextStyle(fontSize: 15),
                                      ),
                                    ],
                                  ),
                                  SizedBox(
                                    height: 20,
                                  ),
                                  Row(
                                    children: [
                                      // SizedBox(
                                      //   width: 10,
                                      // ),
                                      Text(
                                        "PhoneNo : " +
                                            _resultsList[index]['phoneNo'],
                                        style: TextStyle(fontSize: 15),
                                      ),
                                      SizedBox(
                                        width: 30,
                                      ),
                                      Text(
                                        "BloodGroup : " +
                                            _resultsList[index]['bloodGroup'],
                                        style: TextStyle(fontSize: 15),
                                      ),
                                    ],
                                  ),
                                  SizedBox(height: 20),
                                  Row(
                                    children: [
                                      Text(
                                        "Location : " +
                                            _resultsList[index]['address'],
                                        style: TextStyle(fontSize: 15),
                                      ),
                                    ],
                                  )
                                ],
                              ),
                              Column(
                                mainAxisAlignment: MainAxisAlignment.spaceAround,
                                children: [
                                  SizedBox(height: 5,),
                                  IconButton(
                                    onPressed: () {},
                                    icon: Icon(Icons.call),
                                    color: Colors.green,
                                  ),
                                  SizedBox(
                                    height: 3,
                                  ),
                                  IconButton(
                                    onPressed: () {},
                                    icon: Icon(Icons.message_rounded),
                                    color: Colors.amber,
                                  )
                                ],
                              )
                            ]),
                      ),
                    );
                  },
                ),
              )
            ],
          ),
        ));
  }

  getUserSearchDetails() async {
    await Firebase.initializeApp();
    var data = await FirebaseFirestore.instance.collection('userDto').get();
    setState(() {
      _allresults = data.docs;
    });
    searchResulstsList();
    return "complete";
  }
}

class GetUserDetails extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Expanded(
      child: GetUserDetailsFromFirebase(),
    );
  }
}

Future<QuerySnapshot<Object?>> getFirebaseInstance() async {
  await Firebase.initializeApp();
  return await FirebaseFirestore.instance.collection('userDto').get();
}
