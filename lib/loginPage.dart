import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_core/firebase_core.dart';
import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:flutteruthiramv003/donorCreateRequest.dart';
import 'package:flutteruthiramv003/services.dart';

class UserLogin extends StatefulWidget {
  const UserLogin({Key? key}) : super(key: key);

  @override
  _UserLoginState createState() => _UserLoginState();
}

late String _userName;
late String _password;
final _form = GlobalKey<FormState>();
late List<dynamic> userDto;

class _UserLoginState extends State<UserLogin> {
  Services services = new Services();

  var _usernameforForgetPassword;
  @override
  void initState() {
    super.initState();
  }

  void chechUserDetails() async {
    List<dynamic> result;
    try {
      QuerySnapshot snapshot =
          await FirebaseFirestore.instance.collection('userDto').get();
      result = snapshot.docs.map((doc) => doc.data()).toList();
      userDto = result;
      services.setUsers(userDto);
    } catch (error) {
      print(error);
      return null;
    }
    final _isValid = _form.currentState!.validate();
    if (_isValid) {
      if (services.checkUserName(_userName)) {
        if (services.checkPassword(_password)) {
          Fluttertoast.showToast(
              msg: "Login Successful",
              toastLength: Toast.LENGTH_LONG,
              fontSize: 14,
              gravity: ToastGravity.BOTTOM);
          Navigator.pushNamed(context, 'donorCreateRequest',
              arguments: _userName);
        } else {
          Fluttertoast.showToast(
              msg: "Invaid Password!",
              toastLength: Toast.LENGTH_LONG,
              fontSize: 14,
              gravity: ToastGravity.BOTTOM);
        }
      } else {
        Fluttertoast.showToast(
            msg: "User doesn't exists!",
            toastLength: Toast.LENGTH_LONG,
            fontSize: 14,
            gravity: ToastGravity.BOTTOM);
      }
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Uthiram Login Page"),
      ),
      body: Center(
        child: SingleChildScrollView(
          padding: EdgeInsets.all(10),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Form(
                  key: _form,
                  child: Column(
                    children: [
                      TextFormField(
                          validator: (userName) {
                            if (userName!.length != 8) {
                              return "Enter Valid UserName/Rollno";
                            }
                          },
                          onChanged: (userName) {
                            _userName = userName;
                            _usernameforForgetPassword = userName;
                          },
                          decoration: InputDecoration(
                              hintText: "userName",
                              contentPadding: EdgeInsets.symmetric(
                                  vertical: 5.0, horizontal: 32.0),
                              enabledBorder: OutlineInputBorder(
                                borderSide: BorderSide(width: 1),
                                borderRadius: BorderRadius.circular(32.0),
                              ))),
                      SizedBox(
                        height: 30,
                      ),
                      TextFormField(
                          validator: (password) {
                            if (password!.length < 6) {
                              return "Enter valid Password";
                            }
                          },
                          onChanged: (password) {
                            _password = password;
                          },
                          decoration: InputDecoration(
                              hintText: "password",
                              contentPadding: EdgeInsets.symmetric(
                                  vertical: 5.0, horizontal: 32.0),
                              enabledBorder: OutlineInputBorder(
                                borderSide: BorderSide(width: 1),
                                borderRadius: BorderRadius.circular(32.0),
                              ))),
                    ],
                  )),
              SizedBox(
                height: 25,
              ),
              Align(
                alignment: Alignment.centerRight,
                child: MaterialButton(
                  child: Text(
                    "Forget Password?",
                    style: TextStyle(color: Colors.black),
                  ),
                  onPressed: () {
                    Navigator.pushNamed(context, "forgetPassword",
                        arguments: '$_usernameforForgetPassword');
                  },
                  shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(20)),
                  color: Colors.white,
                  minWidth: MediaQuery.of(context).size.width - 150,
                ),
              ),
              SizedBox(
                height: 40,
              ),
              MaterialButton(
                child: Text(
                  "Login",
                  style: TextStyle(fontSize: 16),
                ),
                onPressed: chechUserDetails,
                shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(20)),
                color: Colors.amberAccent,
                minWidth: MediaQuery.of(context).size.width - 50,
              ),
              SizedBox(
                height: 30,
              ),
              MaterialButton(
                child: Text(
                  "Cancel",
                  style: TextStyle(fontSize: 16),
                ),
                onPressed: () {},
                color: Colors.blueAccent,
                shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(20)),
                minWidth: MediaQuery.of(context).size.width - 80,
              ),
              SizedBox(
                height: 30,
              ),
              GestureDetector(
                onTap: () {
                  Navigator.pushNamed(context, "donorRegister");
                },
                child: Text("New Donor? Donate!",
                    style: TextStyle(
                      fontSize: 19,
                      fontStyle: FontStyle.italic,
                      color: Colors.red,
                    )),
              )
            ],
          ),
        ),
      ),
    );
  }
}

Future<QuerySnapshot<Object?>> initiateFirebaseApp() async {
  await Firebase.initializeApp();
  return await FirebaseFirestore.instance.collection('userDto').get();
}
