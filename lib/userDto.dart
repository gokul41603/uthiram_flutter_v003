import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutteruthiramv003/donorCreateRequest.dart';

class UserDto {
  late String donorName;
  late String rollNo;
  late String phoneNo;
  late String age;
  late String weight;
  late String departmentName;
  late String bloodGroup;
  late String districtName;
  late String address;
  late String pincode;

  void setDonorName(String donorName) {
    this.donorName = donorName;
  }

  String getDonorName() {
    return donorName;
  }

  void setRollNo(String rollNo) {
    this.rollNo = rollNo;
  }

  String getRollNo() {
    return rollNo;
  }

  void setPhoneNo(String phoneNo) {
    this.phoneNo = phoneNo;
  }

  String getPhoneNo() {
    return phoneNo;
  }

  void setAge(String age) {
    this.age = age;
  }

  String getAge() {
    return age;
  }

  void setWeight(String weight) {
    this.weight = weight;
  }

  String getWeight() {
    return weight;
  }

  void setDepartmentName(String departmentName) {
    this.departmentName = departmentName;
  }

  String getDepartmentName() {
    return departmentName;
  }

  void setBloodGroup(String bloodGroup) {
    this.bloodGroup = bloodGroup;
  }

  String getBloodGroup() {
    return bloodGroup;
  }

  void setDistrictName(String districtName) {
    this.districtName = districtName;
  }

  String getDistrict() {
    return districtName;
  }

  void setPincode(String pincode) {
    this.pincode = pincode;
  }

  String getPincode() {
    return pincode;
  }

  void setAddress(String address) {
    this.address = address;
  }

  String getAddress() {
    return address;
  }
}
