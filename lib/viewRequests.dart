import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_core/firebase_core.dart';
import 'package:flutter/material.dart';
import 'package:flutteruthiramv003/services.dart';

Object? userName;

class ViewRequests extends StatefulWidget {
  const ViewRequests({Key? key}) : super(key: key);

  @override
  _ViewRequestsState createState() => _ViewRequestsState();
}

class _ViewRequestsState extends State<ViewRequests> {
  @override
  Widget build(BuildContext context) {
    userName = ModalRoute.of(context)!.settings.arguments;
    return Scaffold(
      backgroundColor: Colors.amber.shade50,
      appBar: AppBar(
        title: Text("View Requests"),
      ),
      drawer: Drawer(
        child: Column(
          children: [
            SizedBox(
              height: 60,
            ),
            ListTile(
              leading: Icon(Icons.person),
              title: Text("Donor Profile"),
              onTap: () {
                Navigator.pushNamed(context, "donorProfile",
                    arguments: userName);
              },
            ),
            Divider(
              height: 1,
            ),
            ListTile(
              leading: Icon(Icons.update),
              title: Text("Donor Status"),
              onTap: () {
                Navigator.pushNamed(context, "donorStatus");
              },
            ),
            Divider(
              height: 1,
            ),
            ListTile(
              leading: Icon(Icons.request_page),
              title: Text("Create Request"),
              onTap: () {
                Navigator.pushNamed(context, "donorCreateRequest",
                    arguments: userName);
              },
            ),
            Divider(
              height: 1,
            ),
            ListTile(
              leading: Icon(Icons.list),
              title: Text("View Requests"),
              onTap: () {
                Navigator.pushNamed(context, "viewRequests",
                    arguments: userName);
              },
            ),
            Divider(
              height: 1,
            ),
          ],
        ),
      ),
      body: GetCreateRequestFromFirebase(),
    );
  }
}

class GetCreateRequestFromFirebase extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return FutureBuilder<QuerySnapshot>(
        future: getFirebaseInstanceForCreateRequest(),
        builder: (context, snapshot) {
          if (snapshot.connectionState == ConnectionState.done) {
            var doc = snapshot.data!.docs;
            return ListView.builder(
              itemCount: doc.length,
              itemBuilder: (context, index) {
                return Card(
                  elevation: 5,
                  margin: EdgeInsets.all(2),
                  child: Container(
                    height: 150,
                    width: MediaQuery.of(context).size.width - 20,
                    child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceAround,
                        children: [
                          Column(
                            children: [
                              SizedBox(
                                height: 20,
                              ),
                              Row(
                                children: [
                                  Text(
                                    "PatientName : " +
                                        doc[index]['patientName'],
                                    style: TextStyle(fontSize: 15),
                                  ),
                                  SizedBox(
                                    width: 30,
                                  ),
                                  Text(
                                    "BloodGroup : " + doc[index]['bloodGroup'],
                                    style: TextStyle(fontSize: 15),
                                  ),
                                ],
                              ),
                              SizedBox(
                                height: 20,
                              ),
                              Row(
                                children: [
                                  Text(
                                    "HospitalName : " +
                                        doc[index]['hospitalName'],
                                    style: TextStyle(fontSize: 15),
                                  ),
                                  SizedBox(
                                    width: 30,
                                  ),
                                  Text(
                                    "Location : " +
                                        doc[index]['hospitalLocation'],
                                    style: TextStyle(fontSize: 15),
                                  ),
                                ],
                              ),
                              SizedBox(
                                height: 20,
                              ),
                              Row(
                                children: [
                                  Text(
                                    "NeededWithIn : " +
                                        doc[index]['neededWithIn'],
                                    style: TextStyle(fontSize: 15),
                                  ),
                                ],
                              ),
                              SizedBox(
                                width: 20,
                              ),
                              Row(
                                children: [
                                  Text(
                                    "Contact No : " + doc[index]['contactNo'],
                                    style: TextStyle(fontSize: 15),
                                  ),
                                ],
                              )
                            ],
                          ),
                          Column(
                            children: [
                              IconButton(
                                onPressed: () {},
                                icon: Icon(Icons.call),
                                color: Colors.green,
                              ),
                              SizedBox(
                                height: 3,
                              ),
                              IconButton(
                                onPressed: () {},
                                icon: Icon(Icons.message_rounded),
                                color: Colors.amber,
                              )
                            ],
                          )
                        ]),
                  ),
                );
              },
            );
          }
          return Center(child: CircularProgressIndicator());
        });
  }
}

Future<QuerySnapshot<Object?>> getFirebaseInstanceForCreateRequest() async {
  await Firebase.initializeApp();
  return await FirebaseFirestore.instance
      .collection('DonorRequests')
      .doc('$userName')
      .collection('$userName')
      .get();
}
