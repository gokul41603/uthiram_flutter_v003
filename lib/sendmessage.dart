import 'package:flutter/material.dart';
import 'package:flutter_sms/flutter_sms.dart';

class SendMessage extends StatefulWidget {
  const SendMessage({Key? key}) : super(key: key);

  @override
  _SendMessageState createState() => _SendMessageState();
}

class _SendMessageState extends State<SendMessage> {
  @override
  Widget build(BuildContext context) {
    late String message;
    var donorDetails = ModalRoute.of(context)!.settings.arguments as List;
    return Scaffold(
      appBar: AppBar(title: Text("Send Message")),
      body: Container(
        child: Column(children: [
          SizedBox(
            height: 60,
          ),
          TextField(
            onChanged: (msg) {
              message = msg;
            },
            decoration: InputDecoration(
                hintText: "Type Something...",
                filled: true,
                fillColor: Colors.white,
                contentPadding:
                    EdgeInsets.symmetric(horizontal: 32, vertical: 30),
                enabledBorder: OutlineInputBorder(
                    borderRadius: BorderRadius.circular(32))),
          ),
          SizedBox(
            height: 30,
          ),
          MaterialButton(
            color: Colors.redAccent,
            onPressed: () {
              launchSms(message: message, number: ('tel://${donorDetails[0]}'));
            },
            child: Text(
              "Send to ${donorDetails[1]}",
              style: TextStyle(color: Colors.white),
            ),
          )
        ]),
      ),
    );
  }
}
