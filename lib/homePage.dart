import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_core/firebase_core.dart';
import 'package:flutter/material.dart';
import 'package:flutteruthiramv003/services.dart';

class HomePage extends StatefulWidget {
  const HomePage({Key? key}) : super(key: key);

  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("HomePage"),
      ),
      drawer: Drawer(
        elevation: 10,
        child: Column(
          children: [
            SizedBox(
              height: 90,
            ),
            ListTile(
              leading: Icon(Icons.home),
              title: Text(
                "HomePage",
                style: TextStyle(fontSize: 16),
              ),
              onTap: () {
                Navigator.pushNamed(context, "homePage");
              },
            ),
            Divider(
              height: 1,
            ),
            ListTile(
              leading: Icon(Icons.search),
              title: Text(
                "Donor Search",
                style: TextStyle(fontSize: 16),
              ),
              onTap: () {
                Navigator.pushNamed(context, "donorSearch");
              },
            ),
            Divider(
              height: 1,
            ),
            ListTile(
              leading: Icon(Icons.login),
              title: Text(
                "Login",
                style: TextStyle(fontSize: 16),
              ),
              onTap: () {
                Navigator.pushNamed(context, "login");
              },
            ),
            Divider(
              height: 1,
            ),
            ListTile(
              leading: Icon(Icons.map),
              title: Text(
                "Map",
                style: TextStyle(fontSize: 16),
              ),
              onTap: () {
                Navigator.pushNamed(context, "mapScreen");
              },
            ),
            Divider(
              height: 1,
            )
          ],
        ),
      ),
      body: GetUserDetailsFromFirebase(),
    );
  }
}

Future<QuerySnapshot<Object?>> getFirebaseInstance() async {
  await Firebase.initializeApp();
  return await FirebaseFirestore.instance.collection('DonorRequests').get();
}
