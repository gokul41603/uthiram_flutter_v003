import 'package:firebase_core/firebase_core.dart';
import 'package:flutter/material.dart';
import 'package:flutteruthiramv003/donorCreateRequest.dart';
import 'package:flutteruthiramv003/donorProfile.dart';
import 'package:flutteruthiramv003/donorSearch.dart';
import 'package:flutteruthiramv003/donorStatus.dart';
import 'package:flutteruthiramv003/forgetPassword.dart';
import 'package:flutteruthiramv003/homePage.dart';
import 'package:flutteruthiramv003/loginPage.dart';
import 'package:flutteruthiramv003/registerPage.dart';
import 'package:flutteruthiramv003/sendmessage.dart';
import 'package:flutteruthiramv003/viewRequests.dart';

void main() {
  getFirebaseInstance();
  runApp(MaterialApp(
    routes: {
      "homePage": (context) => HomePage(),
      "login": (context) => UserLogin(),
      "donorRegister": (context) => RegisterPage(),
      "donorProfile": (context) => DonorProfile(),
      "donorCreateRequest": (context) => DonorCreateRequest(),
      "donorSearch": (context) => DonorSearch(),
      "donorStatus": (context) => DonorStatus(),
      "viewRequests": (context) => ViewRequests(),
      "sendMessage": (context) => SendMessage(),
      "forgetPassword": (context) => ForgetPasswordPage(),
      "mapScreen":(context)=> MapScreen()

    },
    home: HomePage(),
    debugShowCheckedModeBanner: false,
  ));
}

Future<void> getFirebaseInstance() async {
  WidgetsFlutterBinding.ensureInitialized();
  await Firebase.initializeApp();
}
