import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';

enum donorStatus { Active, Inactive }
Object? userName;

class DonorStatus extends StatefulWidget {
  const DonorStatus({Key? key}) : super(key: key);

  @override
  _DonorStatusState createState() => _DonorStatusState();
}

class _DonorStatusState extends State<DonorStatus> {
  donorStatus _status = donorStatus.Active;
  var _lastDonatedDateController = TextEditingController();
  @override
  Widget build(BuildContext context) {
    userName = ModalRoute.of(context)!.settings.arguments;
    return Scaffold(
      backgroundColor: Colors.amber.shade50,
      appBar: AppBar(
        title: Text("Donor Status"),
      ),
      drawer: Drawer(
        child: Column(
          children: [
            SizedBox(
              height: 60,
            ),
            ListTile(
              leading: Icon(Icons.person),
              title: Text("Donor Status"),
              onTap: () {},
            ),
            Divider(
              height: 1,
            ),
          ],
        ),
      ),
      body: Center(
        child: SingleChildScrollView(
          child: StreamBuilder<DocumentSnapshot>(
              stream: FirebaseFirestore.instance
                  .collection('userDto')
                  .doc("$userName")
                  .snapshots(),
              builder: (BuildContext context,
                  AsyncSnapshot<DocumentSnapshot> snapshot) {
                if (snapshot.hasData) {
                  return Center(
                    child: Column(
                      children: [
                        Text('Donor Name : ${snapshot.data!['donorName']}'),
                        SizedBox(
                          height: 30,
                        ),
                        TextField(
                            controller: _lastDonatedDateController,
                            decoration: InputDecoration(
                                hintText: "Last Donated date (DD/MM/YYYY)",
                                filled: true,
                                label: Text("Last Donated date (DD/MM/YYYY)"),
                                fillColor: Colors.white,
                                contentPadding: EdgeInsets.symmetric(
                                    vertical: 5.0, horizontal: 32.0),
                                enabledBorder: OutlineInputBorder(
                                  borderSide: BorderSide(width: 1),
                                  borderRadius: BorderRadius.circular(32.0),
                                ))),
                        SizedBox(
                          height: 30,
                        ),
                        ListTile(
                          title: Text('Active'),
                          leading: Radio(
                              value: donorStatus.Active,
                              groupValue: _status,
                              onChanged: (donorStatus? value) {
                                setState(() {
                                  _status = value!;
                                  print(_status.name);
                                });
                              }),
                        ),
                        SizedBox(
                          height: 30,
                        ),
                        ListTile(
                          title: Text("Inactive"),
                          leading: Radio(
                              value: donorStatus.Inactive,
                              groupValue: _status,
                              onChanged: (donorStatus? value) {
                                setState(() {
                                  _status = value!;
                                  print(_status.name);
                                });
                              }),
                        ),
                        SizedBox(
                          height: 30,
                        ),
                        MaterialButton(
                          child: Text("Update Status"),
                          onPressed: () {
                            FirebaseFirestore.instance
                                .collection('userDto')
                                .doc('$userName')
                                .update({
                              'status': _status.name,
                              'lastDonatedDate': _lastDonatedDateController.text
                            });
                          },
                        )
                      ],
                    ),
                  );
                }
                //this will load first
                return CircularProgressIndicator();
              }),
        ),
      ),
    );
  }
}
