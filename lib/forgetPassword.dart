import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';

class ForgetPasswordPage extends StatefulWidget {
  const ForgetPasswordPage({Key? key}) : super(key: key);

  @override
  State<ForgetPasswordPage> createState() => _ForgetPasswordPageState();
}

class _ForgetPasswordPageState extends State<ForgetPasswordPage> {
  var passwordController = TextEditingController();
  var confirmPasswordController = TextEditingController();
  var password;
  var confirmPassword;
  final _form = GlobalKey<FormState>();
  Object? userName;

  void _validateForm() {
    print("UserName : $userName");
    updatePassword();
    updateConfirmPassword();
  }

  CollectionReference userpassword =
      FirebaseFirestore.instance.collection('userDto');

  Future<void> updatePassword() {
    return userpassword
        .doc('$userName')
        .update({'password': '${passwordController.text}'})
        .then((value) => print("Password Updated"))
        .catchError((error) => print("Failed to update password: $error"));
  }

  Future<void> updateConfirmPassword() {
    return userpassword
        .doc('$userName')
        .update({'confirmPassword': '${confirmPasswordController.text}'})
        .then((value) => print("ConfirmPassword Updated"))
        .catchError((error) => print("Failed to update password: $error"));
  }

  @override
  Widget build(BuildContext context) {
    userName = ModalRoute.of(context)!.settings.arguments;
    return Scaffold(
      appBar: AppBar(title: Text("Forget Password")),
      body: SingleChildScrollView(
          child: Column(
        children: [
          TextFormField(
              validator: (password) {
                if (password!.isEmpty) {
                  return "Password can't be Empty";
                }
              },
              onChanged: (password) {
                passwordController.text = password;
              },
              decoration: InputDecoration(
                  hintText: "Password",
                  filled: true,
                  fillColor: Colors.white,
                  contentPadding:
                      EdgeInsets.symmetric(vertical: 5.0, horizontal: 32.0),
                  enabledBorder: OutlineInputBorder(
                    borderSide: BorderSide(width: 1),
                    borderRadius: BorderRadius.circular(32.0),
                  ))),
          SizedBox(
            height: 30,
          ),
          TextFormField(
              validator: (confirmpassword) {
                if (confirmpassword!.isEmpty) {
                  return "Confirm Password can't be Empty";
                }
              },
              onChanged: (confirmpassword) {
                confirmPasswordController.text = confirmpassword;
              },
              decoration: InputDecoration(
                  hintText: "Confirm Password",
                  filled: true,
                  fillColor: Colors.white,
                  contentPadding:
                      EdgeInsets.symmetric(vertical: 5.0, horizontal: 32.0),
                  enabledBorder: OutlineInputBorder(
                    borderSide: BorderSide(width: 1),
                    borderRadius: BorderRadius.circular(32.0),
                  ))),
          SizedBox(
            height: 50,
          ),
          MaterialButton(
            elevation: 10,
            minWidth: MediaQuery.of(context).size.width - 40,
            shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(32.0)),
            onPressed: _validateForm,
            color: Colors.red,
            child: Text(
              "Update Password",
              style: TextStyle(color: Colors.white),
            ),
          )
        ],
      )),
    );
  }
}
