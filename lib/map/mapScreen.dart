import 'dart:async';

import 'package:flutter/material.dart';
import 'package:geolocator/geolocator.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:uthiram/maps/global.dart';
class MapScreen extends StatefulWidget
{
  @override
  _MapScreenState createState() => _MapScreenState();
}




class _MapScreenState extends State<MapScreen>
{
  final Completer<GoogleMapController> _controllerGoogleMap = Completer();
  GoogleMapController? newGoogleMapController;

  static const CameraPosition _kGooglePlex = CameraPosition(
    target: LatLng(37.42796133580664, -122.085749655962),
    zoom: 14.4746,
  );
 GlobalKey<ScaffoldState> sKey=GlobalKey<ScaffoldState>();
 double searchLocationContainerHeight= 200.0;

 Position? userCurrentPosition;
 var geoLocator =Geolocator();
 LocationPermission? _locationPermission;

 checkIfLocationPermissionAllowed() async
 {
   _locationPermission = await Geolocator.requestPermission();
   if(_locationPermission==LocationPermission.denied){
     _locationPermission= await Geolocator.requestPermission();
   }
 }
 locateUserPosition()async{
    Position cPosition = await Geolocator.getCurrentPosition(desiredAccuracy:LocationAccuracy.high );
    userCurrentPosition=cPosition;

    LatLng latlngPosition=LatLng(userCurrentPosition!.latitude,userCurrentPosition!.longitude);
  CameraPosition cameraPosition=CameraPosition(target: latlngPosition,zoom:14);
newGoogleMapController!.animateCamera(CameraUpdate.newCameraPosition(cameraPosition));
 }

 @override
 void initState(){
   super.initState();
checkIfLocationPermissionAllowed();

 }
  get onPressed => null;
  @override
  Widget build(BuildContext context)
  {
    return Scaffold(
      body: Stack(
        children: [
          GoogleMap(
            mapType: MapType.normal,
            myLocationEnabled: true,
            zoomGesturesEnabled: true,
            zoomControlsEnabled: true,
            initialCameraPosition: _kGooglePlex,
            onMapCreated: (GoogleMapController controller)
            {
              _controllerGoogleMap.complete(controller);
              newGoogleMapController = controller;
              locateUserPosition();
              //for black theme google map
            //  blackThemeGoogleMap();
            },
          ),
         Positioned(
           bottom: 0,
           left: 0,
           right: 0,
           child: AnimatedSize(
             curve:Curves.easeIn,
             duration: Duration(milliseconds: 120),
           child: Container(
             height: searchLocationContainerHeight,
             decoration: BoxDecoration(
               color: Colors.black54,
               borderRadius: BorderRadius.only(
                 topRight:Radius.circular(20),
                 topLeft: Radius.circular(20),
               )
             ),
             child: Padding(
               padding: const EdgeInsets.symmetric(horizontal: 24,vertical: 18),
               child: Column(
                 children: [
                   //from
                   Row(
                     children: [
                       const Icon(Icons.add_location_alt_outlined,color: Colors.grey,),
                     const SizedBox(width:12.0,),
                       Column(
                         crossAxisAlignment: CrossAxisAlignment.start,
                         children: [
                           Text("From",style: TextStyle(color: Colors.grey,fontSize:12),),
                           Text("current location",style: TextStyle(color: Colors.grey,fontSize:12),),
                         ],
                       ),

                     ],
                   ),
                    const SizedBox( height: 10.0),
                const Divider(
                  height: 1,
                  thickness: 1,
                  color:Colors.grey,
                ),
               const SizedBox(height: 16.0),
                   //TO
                   Row(
                     children: [
                       const Icon(Icons.add_location_alt_outlined,color: Colors.grey,),
                       const SizedBox(width:12.0,),
                       Column(
                         crossAxisAlignment: CrossAxisAlignment.start,

                         children: [
                           Text("To",style: TextStyle(color: Colors.grey,fontSize:12),),
                           Text("Where to go?",style: TextStyle(color: Colors.grey,fontSize:12),),
                         ],
                       ),

                     ],
                   ),
                   const SizedBox( height: 10.0),
                   const Divider(
                     height: 1,
                     thickness: 1,
                     color:Colors.grey,
                   ),
                   const SizedBox(height: 16.0),
                   ElevatedButton(onPressed: onPressed, child: const Text(
                     "Go"
                   ),
                     style: ElevatedButton.styleFrom(
                         primary: Colors.white,
                         textStyle:const TextStyle(fontSize: 16,fontWeight: FontWeight.bold),
                   ),
                   ),
                 ],
               ),
             ),
           ),
           ),

),
        ],
      ),
    );
  }
}
