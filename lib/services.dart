import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_core/firebase_core.dart';
import 'package:flutter_sms/flutter_sms.dart';
import 'package:flutteruthiramv003/donorCreateRequest.dart';
import 'package:flutteruthiramv003/homePage.dart';
import 'package:flutteruthiramv003/loginPage.dart';
import 'package:flutteruthiramv003/userDto.dart';
import 'package:flutter/material.dart';
import 'package:url_launcher/url_launcher.dart';

class Services {
  List<dynamic>? userDto;
  UserDto user = new UserDto();
  void setUsers(List<dynamic> usersList) {
    userDto = usersList;
  }

  bool checkUserName(String userName) {
    for (int i = 0; i < userDto!.length; i++) {
      if (userDto![i]['rollNo'] == userName) {
        return true;
      }
    }
    return false;
  }

  bool checkPassword(String password) {
    for (int i = 0; i < userDto!.length; i++) {
      if (userDto![i]['password'] == password) {
        return true;
      }
    }
    return false;
  }
}

class GetUserDetailsFromFirebase extends StatelessWidget {
  late String message;
  @override
  Widget build(BuildContext context) {
    return FutureBuilder<QuerySnapshot>(
        future: getFirebaseInstanceForHomePage(),
        builder: (context, snapshot) {
          if (snapshot.connectionState == ConnectionState.done) {
            var doc = snapshot.data!.docs;
            return ListView.builder(
              itemCount: doc.length,
              itemBuilder: (context, index) {
                return Card(
                  elevation: 5,
                  margin: EdgeInsets.all(2),
                  child: Container(
                    height: 180,
                    width: MediaQuery.of(context).size.width - 20,
                    child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceAround,
                        children: [
                          Column(
                            children: [
                              SizedBox(
                                height: 20,
                              ),
                              Row(
                                children: [
                                  Text(
                                    "Patient Name : " +
                                        doc[index]['patientName'],
                                    style: TextStyle(fontSize: 15),
                                  ),
                                  SizedBox(
                                    width: 30,
                                  ),
                                  Text(
                                    "BloodGroup : " + doc[index]['bloodGroup'],
                                    style: TextStyle(fontSize: 15),
                                  ),
                                ],
                              ),
                              SizedBox(
                                height: 20,
                              ),
                              Row(
                                children: [
                                  Text(
                                    "Contact No : " + doc[index]['contactNo'],
                                    style: TextStyle(fontSize: 15),
                                  ),
                                  SizedBox(
                                    width: 30,
                                  ),
                                  Text(
                                    "Hospital Name : " +
                                        doc[index]['hospitalName'],
                                    style: TextStyle(fontSize: 15),
                                  ),
                                ],
                              ),
                              SizedBox(
                                height: 20,
                              ),
                              Row(
                                children: [
                                  Text(
                                    "Location : " +
                                        doc[index]['hospitalLocation'],
                                    style: TextStyle(fontSize: 15),
                                  )
                                ],
                              ),
                              SizedBox(
                                height: 20,
                              ),
                              Row(
                                children: [
                                  Text(
                                    "Units Needed : " +
                                        doc[index]['unitsNeeded'],
                                    style: TextStyle(fontSize: 15),
                                  )
                                ],
                              ),
                              SizedBox(
                                height: 20,
                              ),
                              Row(
                                children: [
                                  Text(
                                    "Needed WithIn : " +
                                        doc[index]['neededWithIn'],
                                    style: TextStyle(fontSize: 15),
                                  )
                                ],
                              )
                            ],
                          ),
                          Column(
                            children: [
                              IconButton(
                                onPressed: () {
                                  launch(('tel://${doc[index]['contactNo']}'));
                                },
                                icon: Icon(Icons.call),
                                color: Colors.green,
                              ),
                              SizedBox(
                                height: 3,
                              ),
                              IconButton(
                                onPressed: () {
                                  Navigator.pushNamed(context, "sendMessage",
                                      arguments: [
                                        doc[index]['contactNo'],
                                        doc[index]['patientNamhe']
                                      ]);
                                },
                                icon: Icon(Icons.message_rounded),
                                color: Colors.amber,
                              )
                            ],
                          )
                        ]),
                  ),
                );
              },
            );
          }
          return Center(child: CircularProgressIndicator());
        });
  }
}

Future<QuerySnapshot<Object?>> getFirebaseInstanceForHomePage() async {
  await Firebase.initializeApp();
  return await FirebaseFirestore.instance.collection('emergencyRequests').get();
}
