import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:flutteruthiramv003/createRequestDto.dart';
import 'package:flutteruthiramv003/loginPage.dart';
import 'package:flutteruthiramv003/services.dart';

late String userName;

class DonorCreateRequest extends StatefulWidget {
  Services services = new Services();
  @override
  _DonorCreateRequestState createState() => _DonorCreateRequestState();
}

class _DonorCreateRequestState extends State<DonorCreateRequest> {
  Services services = new Services();
  Map<String, dynamic> createRequest = {};
  CollectionReference reference =
      FirebaseFirestore.instance.collection("DonorRequests");
  var findLocationAddress;
  @override
  Widget build(BuildContext context) {
    Object? userName = ModalRoute.of(context)!.settings.arguments;

    return Scaffold(
      backgroundColor: Colors.amber.shade50,
      appBar: AppBar(
        title: Text("Create Request"),
      ),
      drawer: Drawer(
        child: Column(
          children: [
            SizedBox(
              height: 60,
            ),
            ListTile(
              leading: Icon(Icons.person),
              title: Text("Donor Profile"),
              onTap: () {
                Navigator.pushNamed(context, "donorProfile",
                    arguments: userName);
              },
            ),
            Divider(
              height: 1,
            ),
            ListTile(
              leading: Icon(Icons.update),
              title: Text("Donor Status"),
              onTap: () {
                Navigator.pushNamed(context, "donorStatus",
                    arguments: userName);
              },
            ),
            Divider(
              height: 1,
            ),
            ListTile(
              leading: Icon(Icons.request_page),
              title: Text("Create Request"),
              onTap: () {
                Navigator.pushNamed(context, "donorCreateRequest",
                    arguments: userName);
              },
            ),
            Divider(
              height: 1,
            ),
            ListTile(
              leading: Icon(Icons.list),
              title: Text("View Requests"),
              onTap: () {
                Navigator.pushNamed(context, "viewRequests",
                    arguments: userName);
              },
            ),
            Divider(
              height: 1,
            ),
          ],
        ),
      ),
      body: Center(
        child: SingleChildScrollView(
          padding: EdgeInsets.all(10),
          child: Column(
            children: [
              TextField(
                onChanged: (patientName) {
                  createRequest["patientName"] = patientName;
                },
                decoration: InputDecoration(
                    hintText: "Patient Name",
                    filled: true,
                    fillColor: Colors.white,
                    contentPadding:
                        EdgeInsets.symmetric(horizontal: 32, vertical: 10),
                    enabledBorder: OutlineInputBorder(
                        borderRadius: BorderRadius.circular(32))),
              ),
              SizedBox(
                height: 20,
              ),
              TextField(
                onChanged: (bloodGroup) {
                  createRequest['bloodGroup'] = bloodGroup;
                },
                decoration: InputDecoration(
                    hintText: "Blood Group",
                    filled: true,
                    fillColor: Colors.white,
                    contentPadding:
                        EdgeInsets.symmetric(horizontal: 32, vertical: 10),
                    enabledBorder: OutlineInputBorder(
                        borderRadius: BorderRadius.circular(32))),
              ),
              SizedBox(
                height: 20,
              ),
              TextField(
                onChanged: (unitsNeeded) {
                  createRequest['unitsNeeded'] = unitsNeeded;
                },
                decoration: InputDecoration(
                    hintText: "Blood Needed",
                    filled: true,
                    fillColor: Colors.white,
                    contentPadding:
                        EdgeInsets.symmetric(horizontal: 32, vertical: 10),
                    enabledBorder: OutlineInputBorder(
                        borderRadius: BorderRadius.circular(32))),
              ),
              SizedBox(
                height: 20,
              ),
              TextField(
                onChanged: (neededWithIn) {
                  createRequest['neededWithIn'] = neededWithIn;
                },
                decoration: InputDecoration(
                    hintText: "Needed With Time/Date",
                    filled: true,
                    fillColor: Colors.white,
                    contentPadding:
                        EdgeInsets.symmetric(horizontal: 32, vertical: 10),
                    enabledBorder: OutlineInputBorder(
                        borderRadius: BorderRadius.circular(32))),
              ),
              SizedBox(
                height: 20,
              ),
              TextField(
                onChanged: (hospitalName) {
                  createRequest['hospitalName'] = hospitalName;
                },
                decoration: InputDecoration(
                    hintText: "Hospital Name",
                    filled: true,
                    fillColor: Colors.white,
                    contentPadding:
                        EdgeInsets.symmetric(horizontal: 32, vertical: 10),
                    enabledBorder: OutlineInputBorder(
                        borderRadius: BorderRadius.circular(32))),
              ),
              SizedBox(
                height: 20,
              ),
              TextField(
                onChanged: (hospitalLocation) {
                  createRequest['hospitalLocation'] = hospitalLocation;
                  findLocationAddress = hospitalLocation;
                },
                decoration: InputDecoration(
                    hintText: "Hospital Location",
                    filled: true,
                    fillColor: Colors.white,
                    contentPadding:
                        EdgeInsets.symmetric(horizontal: 32, vertical: 10),
                    enabledBorder: OutlineInputBorder(
                        borderRadius: BorderRadius.circular(32))),
              ),
              SizedBox(
                height: 20,
              ),
              TextField(
                onChanged: (contactNo) {
                  createRequest['contactNo'] = contactNo;
                },
                decoration: InputDecoration(
                    hintText: "Contact No",
                    filled: true,
                    fillColor: Colors.white,
                    contentPadding:
                        EdgeInsets.symmetric(horizontal: 32, vertical: 10),
                    enabledBorder: OutlineInputBorder(
                        borderRadius: BorderRadius.circular(32))),
              ),
              SizedBox(
                height: 20,
              ),
              MaterialButton(
                elevation: 10,
                height: 40,
                minWidth: MediaQuery.of(context).size.width - 40,
                shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(32.0)),
                onPressed: () {
                  FirebaseFirestore.instance
                      .collection("DonorRequests")
                      .doc(userName as String)
                      .collection(userName as String)
                      .doc()
                      .set(createRequest);
                  // reference.doc(userName as String).set(createRequest);
                  FirebaseFirestore.instance
                      .collection("emergencyRequests")
                      .doc()
                      .set(createRequest);
                  findLocationAddress();
                },
                color: Colors.red,
                child: Text(
                  "Request Donor ",
                  style: TextStyle(color: Colors.white, fontSize: 16),
                ),
              )
            ],
          ),
        ),
      ),
    );
  }
}
