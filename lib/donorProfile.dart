import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_core/firebase_core.dart';
import 'package:flutter/material.dart';
import 'package:flutteruthiramv003/donorCreateRequest.dart';
import 'package:flutteruthiramv003/services.dart';
import 'package:flutteruthiramv003/userDto.dart';
import 'package:url_launcher/url_launcher.dart';

Map<String, dynamic> donorDetails = {};
Object? userName;
var addressctr = TextEditingController();
var agectr = TextEditingController();
var namectr = TextEditingController();
var weightctr = TextEditingController();
var pincodectr = TextEditingController();

class DonorProfile extends StatefulWidget {
  const DonorProfile({Key? key}) : super(key: key);

  @override
  _DonorProfileState createState() => _DonorProfileState();
}

class _DonorProfileState extends State<DonorProfile> {
  late String donorRollNo;
  Services services = new Services();
  UserDto user = new UserDto();
  CollectionReference reference =
      FirebaseFirestore.instance.collection("userDto");
  var donorNameController = TextEditingController();
  var donorAgeController = TextEditingController();
  var weightController = TextEditingController();
  var addressController = TextEditingController();
  var departmentController = TextEditingController();
  var bloodGroupController = TextEditingController();
  var districtNameController = TextEditingController();

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    Object? userName = ModalRoute.of(context)!.settings.arguments;
    // print("first username : $userName");
    // fetchAllContact(userName);
    return Scaffold(
      backgroundColor: Colors.amber.shade50,
      appBar: AppBar(
        title: Text("Donor Profile"),
      ),
      drawer: Drawer(
        child: Column(
          children: [
            SizedBox(
              height: 60,
            ),
            ListTile(
              leading: Icon(Icons.person),
              title: Text("Donor Profile"),
              onTap: () {},
            ),
            Divider(
              height: 1,
            ),
            ListTile(
              leading: Icon(Icons.update),
              title: Text("Donor Status"),
              onTap: () {},
            ),
            Divider(
              height: 1,
            ),
            ListTile(
              leading: Icon(Icons.request_page),
              title: Text("Create Request"),
              onTap: () {
                Navigator.pushNamed(context, "donorCreateRequest",
                    arguments: userName);
              },
            ),
            Divider(
              height: 1,
            ),
            ListTile(
              leading: Icon(Icons.list),
              title: Text("View Requests"),
              onTap: () {},
            ),
            Divider(
              height: 1,
            ),
          ],
        ),
      ),
      body: SingleChildScrollView(
        child: StreamBuilder<DocumentSnapshot>(
            stream: FirebaseFirestore.instance
                .collection('userDto')
                .doc('$userName')
                .snapshots(),
            builder: (BuildContext context,
                AsyncSnapshot<DocumentSnapshot> snapshot) {
              if (snapshot.hasData) {
                donorNameController.text = snapshot.data!['donorName'];
                donorAgeController.text = snapshot.data!['age'];
                weightController.text = snapshot.data!['weight'];
                bloodGroupController.text = snapshot.data!['bloodGroup'];
                districtNameController.text = snapshot.data!['districtName'];
                departmentController.text = snapshot.data!['departmentName'];
                addressController.text = snapshot.data!['address'];

                donorDetails['donorName'] = snapshot.data!['donorName'];
                donorDetails['age'] = snapshot.data!['age'];
                donorDetails['weight'] = snapshot.data!['weight'];
                donorDetails['bloodGroup'] = snapshot.data!['bloodGroup'];
                donorDetails['districtName'] = snapshot.data!['districtName'];
                donorDetails['departmentName'] =
                    snapshot.data!['departmentName'];
                donorDetails['address'] = snapshot.data!['address'];
                donorDetails['pinCode'] = snapshot.data!['pinCode'];
                donorDetails['rollNo'] = snapshot.data!['rollNo'];
                donorDetails['confirmPassword'] =
                    snapshot.data!['confirmPassword'];
                donorDetails['password'] = snapshot.data!['password'];
                donorDetails['phoneNo'] = snapshot.data!['phoneNo'];
                return Column(
                  children: [
                    SizedBox(
                      height: 40,
                    ),
                    Padding(
                      padding: EdgeInsets.all(3),
                      child: TextField(
                          controller: donorNameController,
                          decoration: InputDecoration(
                              hintText: "Donor Name",
                              filled: true,
                              fillColor: Colors.white,
                              label: Text('Donor Name'),
                              contentPadding: EdgeInsets.symmetric(
                                  vertical: 5.0, horizontal: 32.0),
                              enabledBorder: OutlineInputBorder(
                                borderSide: BorderSide(width: 1),
                                borderRadius: BorderRadius.circular(32.0),
                              ))),
                    ),
                    SizedBox(
                      height: 30,
                    ),
                    TextField(
                        controller: donorAgeController,
                        onChanged: (age) {
                          donorDetails['age'] = age;
                        },
                        decoration: InputDecoration(
                            hintText: "Donor Age",
                            filled: true,
                            fillColor: Colors.white,
                            label: Text('Donor Age'),
                            contentPadding: EdgeInsets.symmetric(
                                vertical: 5.0, horizontal: 32.0),
                            enabledBorder: OutlineInputBorder(
                              borderSide: BorderSide(width: 1),
                              borderRadius: BorderRadius.circular(32.0),
                            ))),
                    SizedBox(
                      height: 30,
                    ),
                    TextField(
                        controller: weightController,
                        onChanged: (weight) {
                          donorDetails['weight'] = weight;
                        },
                        decoration: InputDecoration(
                            hintText: "Donor Weight",
                            filled: true,
                            fillColor: Colors.white,
                            label: Text('Donor Weight'),
                            contentPadding: EdgeInsets.symmetric(
                                vertical: 5.0, horizontal: 32.0),
                            enabledBorder: OutlineInputBorder(
                              borderSide: BorderSide(width: 1),
                              borderRadius: BorderRadius.circular(32.0),
                            ))),
                    SizedBox(
                      height: 30,
                    ),
                    TextField(
                        controller: addressController,
                        onChanged: (address) {
                          donorDetails['address'] = address;
                        },
                        decoration: InputDecoration(
                            hintText: "Donor Address",
                            filled: true,
                            fillColor: Colors.white,
                            label: Text('Donor Address'),
                            contentPadding: EdgeInsets.symmetric(
                                vertical: 5.0, horizontal: 32.0),
                            enabledBorder: OutlineInputBorder(
                              borderSide: BorderSide(width: 1),
                              borderRadius: BorderRadius.circular(32.0),
                            ))),
                    SizedBox(
                      height: 30,
                    ),
                    Center(
                        child:
                            Text('Blood Group : ${bloodGroupController.text}')),
                    SizedBox(
                      height: 30,
                    ),
                    Center(
                      child: Text(
                          "District Name :  ${districtNameController.text}"),
                    ),
                    SizedBox(
                      height: 30,
                    ),
                    Center(
                        child: Text(
                            'Department Name : ${departmentController.text}')),
                    SizedBox(
                      height: 30,
                    ),
                    Center(
                        child: Text('Roll No : ${snapshot.data!['rollNo']}')),
                    SizedBox(
                      height: 30,
                    ),
                    Center(
                        child: MaterialButton(
                      onPressed: () {
                        FirebaseFirestore.instance
                            .collection('userDto')
                            .doc('$userName')
                            .update(donorDetails);
                      },
                      color: Colors.blueAccent,
                      child: Text(
                        "Update Profile",
                        style: TextStyle(color: Colors.white),
                      ),
                    )),
                  ],
                );
              }
              //this will load first
              return CircularProgressIndicator();
            }),
      ),
    );
  }

  Future<QuerySnapshot<Object?>> getFirebaseInstance() async {
    await Firebase.initializeApp();
    return await FirebaseFirestore.instance.collection('userDto').get();
  }
}
